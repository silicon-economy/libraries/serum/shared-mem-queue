// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

#![no_std]

//! This library implements a simple single-writer single-reader queue which may be used for
//! inter-processor-communication over a shared memory region. Initially, this has been developed as
//! a simple solution with minimal overhead to communicate between the Cortex-M4 and the Cortex-A7
//! on STM32MP1 microprocessors but it may also be useful in different scenarios though.
//!
//! # Implementation details
//! The queue operates on a shared memory region and keeps track of a write- and a read-pointer.
//! To access both pointers from both processors, the write- and read-pointers
//! are stored in the shared memory region itself so the capacity of the queue
//! is `2*size_of::<usize>()` smaller than the memory region size.
//!
//! The main contract here is that only the writer may write to the
//! write-pointer, only the reader may write to the read-pointer, the memory
//! region in front of the write-pointer and up to the read-pointer is owned by
//! the writer, the memory region in front of the read-pointer and up to the
//! write-pointer is owned by the reader. For initialization, both pointers have
//! to be set to 0 at the beginning. This breaks the contract so it has to be
//! done by processor A when it is guaranteed that processor B does not access
//! the queue yet to prevent race conditions.
//!
//! Because processor A has to initialize the queue and processor B should not
//! reset the write- and read-pointers, there are two methods for
//! initialization: `create()` should be called by the first processor and
//! sets both pointers to 0, `attach()` should be called by the second one.
//!
//! The `SharedMemQueue` implements both the write- and the read-methods but
//! each processor should have either the writing side or the reading side
//! assigned to it and must not call the other methods. It would also be
//! possible to have a `SharedMemWriter` and a `SharedMemReader` but this
//! design was initially chosen so that the queue can also be used as a simple
//! ring buffer on a single processor.
//!
//! # Usage Examples
//!
//! ## Single-Processor Ring-Buffer
//! ```
//! # use shared_mem_queue::SharedMemQueue;
//! let mut buffer = [0u8; 100];
//! let mut queue = unsafe { SharedMemQueue::create(buffer.as_mut_ptr(), 100) };
//! let tx = [1, 2, 3, 4];
//! queue.blocking_write(&tx);
//! let mut rx = [0u8; 4];
//! queue.blocking_read(&mut rx);
//! assert_eq!(&tx, &rx);
//! ```
//!
//! ## Single-Processor Queue
//!
//! A more realistic example involves creating a reader and a writer separately:
//! ```
//! # use shared_mem_queue::SharedMemQueue;
//! let mut buffer = [0u8; 100];
//! let mut writer = unsafe { SharedMemQueue::create(buffer.as_mut_ptr(), 100) };
//! let mut reader = unsafe { SharedMemQueue::attach(buffer.as_mut_ptr(), 100) };
//! let tx = [1, 2, 3, 4];
//! writer.blocking_write(&tx);
//! let mut rx = [0u8; 4];
//! reader.blocking_read(&mut rx);
//! assert_eq!(&tx, &rx);
//! ```
//!
//! ## Shared-Memory Queue
//!
//! In general, an `mmap` call is required to access the queue from a Linux system. This can be
//! done with the [`memmap` crate](https://crates.io/crates/memmap). The following example probably
//! panics when executed naively because access to `/dev/mem` requires root
//! privileges. Additionally, the example region in use is probably not viable for this
//! queue on most systems:
//! ```no_run
//! # use shared_mem_queue::SharedMemQueue;
//! # use std::convert::TryInto;
//! let shared_mem_start = 0x10048000; // example
//! let shared_mem_len = 0x00008000;   // region
//! let dev_mem = std::fs::OpenOptions::new()
//!     .read(true)
//!     .write(true)
//!     .open("/dev/mem")
//!     .expect("Could not open /dev/mem, do you have root privileges?");
//! let mut mmap = unsafe {
//!     memmap::MmapOptions::new()
//!         .len(shared_mem_len)
//!         .offset(shared_mem_start.try_into().unwrap())
//!         .map_mut(&dev_mem)
//!         .unwrap()
//! };
//! let mut channel = unsafe {
//!     SharedMemQueue::attach(mmap.as_mut_ptr(), shared_mem_len)
//! };
//! ```
//!
//! ## Bi-Directional Shared-Memory Communication
//!
//! In most inter-processor-communication scenarios, two queues will be required for bi-directional
//! communication. A single `mmap` call is sufficient, the memory region can be split manually
//! afterwards:
//! ```no_run
//! # use shared_mem_queue::SharedMemQueue;
//! # use std::convert::TryInto;
//! let shared_mem_start = 0x10048000; // example
//! let shared_mem_len = 0x00008000;   // region
//! let dev_mem = std::fs::OpenOptions::new()
//!     .read(true)
//!     .write(true)
//!     .open("/dev/mem")
//!     .expect("Could not open /dev/mem, do you have root privileges?");
//! let mut mmap = unsafe {
//!     memmap::MmapOptions::new()
//!         .len(shared_mem_len)
//!         .offset(shared_mem_start.try_into().unwrap())
//!         .map_mut(&dev_mem)
//!         .unwrap()
//! };
//! let mut channel_write = unsafe {
//!     SharedMemQueue::attach(mmap.as_mut_ptr(), shared_mem_len / 2)
//! };
//! let mut channel_read = unsafe {
//!     SharedMemQueue::attach(mmap.as_mut_ptr().add(shared_mem_len / 2), shared_mem_len / 2)
//! };
//! ```
//!
//! # License
//!
//! Open Logistics Foundation License\
//! Version 1.3, January 2023
//!
//! See the LICENSE file in the top-level directory.
//!
//! # Contact
//!
//! Fraunhofer IML Embedded Rust Group - <embedded-rust@iml.fraunhofer.de>

use core::convert::TryFrom;
use core::ptr::read_volatile;
use core::ptr::write_volatile;
use core::sync::atomic;
use core::sync::atomic::Ordering;

/// The main queue type. Read the crate documentation for further information and usage examples.
pub struct SharedMemQueue {
    write_pos_ptr: *mut usize,
    read_pos_ptr: *mut usize,
    data_ptr: *mut u8,
    capacity: usize,
}
// TODO I have no idea if there is a better way to enable using SharedMemQueue from a thread
// but I'm sure that there is a way...
unsafe impl Send for SharedMemQueue {}

impl SharedMemQueue {
    /// Creates a new queue in the given memory region and initializes both pointers.
    ///
    /// # Safety
    /// This method has to be called before the other processor tries to access the queue because
    /// the other processor might access an uninitialized memory region otherwise which will most
    /// likely result in crashes.
    ///
    /// Obviously, the memory pointer and the memory region length must be correct, reserved for
    /// this purpose and known to the other processor.
    pub unsafe fn create(mem: *mut u8, mem_len: usize) -> Self {
        let mut slf = Self::attach(mem, mem_len);
        slf.set_write_pos(0);
        slf.set_read_pos(0);
        slf
    }
    /// Attaches to a queue which has previously been initialized by [`SharedMemQueue::create`],
    /// possibly by an other processor.
    ///
    /// # Safety
    /// This method must not be called before the other processor has properly initialized the
    /// queue because this will most likely result in crashes.
    ///
    /// Obviously, the memory pointer rand the memory region length must be correct, reserved for
    /// this purpose and known to the other processor.
    pub unsafe fn attach(mem: *mut u8, mem_len: usize) -> Self {
        SharedMemQueue {
            write_pos_ptr: mem as *mut usize,
            read_pos_ptr: (mem as *mut usize).offset(1),
            data_ptr: mem.offset(
                isize::try_from(2 * core::mem::size_of::<usize>())
                    .expect("~8u should be convertible to isize"),
            ),
            capacity: mem_len - 2 * core::mem::size_of::<usize>(),
        }
    }
    fn get_write_pos(&self) -> usize {
        unsafe { read_volatile(self.write_pos_ptr) }
    }
    fn get_read_pos(&self) -> usize {
        unsafe { read_volatile(self.read_pos_ptr) }
    }
    fn set_write_pos(&mut self, wpos: usize) {
        unsafe { write_volatile(self.write_pos_ptr, wpos) }
    }
    fn set_read_pos(&mut self, rpos: usize) {
        unsafe { write_volatile(self.read_pos_ptr, rpos) }
    }
    pub fn space(&self) -> usize {
        (self.capacity + self.get_read_pos() - self.get_write_pos() - 1) % self.capacity
    }
    pub fn size(&self) -> usize {
        (self.capacity + self.get_write_pos() - self.get_read_pos()) % self.capacity
    }
    pub fn blocking_write(&mut self, data: &[u8]) {
        loop {
            if self.space() >= data.len() {
                break;
            }
        }
        atomic::fence(Ordering::Acquire);
        let wpos = self.get_write_pos();
        for (i, byte) in data.iter().enumerate() {
            let offset = (wpos + i) % self.capacity;
            unsafe {
                let dptr = self.data_ptr.add(offset);
                write_volatile(dptr, *byte);
            }
        }
        atomic::fence(Ordering::Release);
        let wpos = (wpos + data.len()) % self.capacity;
        self.set_write_pos(wpos);
    }
    pub fn blocking_read(&mut self, buf: &mut [u8]) {
        loop {
            if self.size() >= buf.len() {
                break;
            }
        }
        atomic::fence(Ordering::Acquire);
        let rpos = self.get_read_pos();
        for (i, byte) in buf.iter_mut().enumerate() {
            let offset = (rpos + i) % self.capacity;
            unsafe {
                let dptr = self.data_ptr.add(offset);
                *byte = read_volatile(dptr);
            }
        }
        atomic::fence(Ordering::Release);
        let rpos = (rpos + buf.len()) % self.capacity;
        self.set_read_pos(rpos);
    }
}

impl core::fmt::Write for SharedMemQueue {
    fn write_str(&mut self, s: &str) -> Result<(), core::fmt::Error> {
        self.blocking_write(s.as_bytes());
        Ok(())
    }
}

#[cfg(test)]
mod tests {
    use super::SharedMemQueue;
    #[test]
    fn write_read() {
        let mut buffer = [123u8; 100];
        let mut writer = unsafe { SharedMemQueue::create(buffer.as_mut_ptr(), 100) };
        let mut reader = unsafe { SharedMemQueue::attach(buffer.as_mut_ptr(), 100) };
        let tx = [1, 2, 3, 4];
        writer.blocking_write(&tx);
        let mut rx = [0u8; 4];
        reader.blocking_read(&mut rx);
        assert_eq!(&tx, &rx);
    }
}
