# Changelog

## v0.1.0 - 2023-03-15
- Blocking write and read
- `core::fmt::Write` implementation to use formatting macros like `writeln!`
